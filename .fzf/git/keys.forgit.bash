#!/bin/bash
bind '"\er": redraw-current-line'
bind '"\C-gc"':"\"\$(gco) 2>&-\C-m\""
bind '"\C-ga"':"\"\$(ga)\C-m\""
bind '"\C-gl"':"\"\$(glo)\C-m\""
bind '"\C-gs"':"\"\$(gss)\C-m\""
bind '"\C-gd"':"\"\$(gd)\C-m\""

