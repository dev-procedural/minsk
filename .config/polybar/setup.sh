#!/usr/bin/env bash

# Install script for polybar themes

# Dirs
DIR="$HOME/.config/polybar"
FDIR="$HOME/.local/share/fonts"

# Install Fonts
install_fonts() {
	echo -e "\n[*] Installing fonts..."
	if [[ -d "$FDIR" ]]; then
		cp -rf $DIR/fonts/* "$FDIR"
	else
		mkdir -p "$FDIR"
		cp -rf $DIR/fonts/* "$FDIR"
	fi
}

# Main
install_fonts

adapters=$(ip -c -br a | nl | grep -Eo "^.*\d*  [a-zA-Z0-9 ]*" | grep -iE "(en|wl)+" | awk '{ print $2 }')

wifi=$(echo "$adapters" | grep -i wl)
eth=$(echo "$adapters" | grep -i enp)

sed -r "/module\/wired-network/,/interface/ s|\(interface = \).*|\1 $eth|g"  forest/modules.ini 
sed -r "/module\/wireless-network/,/interface/ s|\(interface = \).*|\1 $wifi|g"  forest/modules.ini 
sed -r "/module\/network/,/interface/ s|\(interface = \).*|\1 $wifi|g"  forest/modules.ini 



