
tv=$(xrandr | grep -iE "hdmi|eDP-1" | grep -iE "\bconnec" | awk '{print $1}')
lap=$(xrandr | grep -i edp | grep -iE "\bconnec" | awk '{print $1}')
thunder=$(xrandr | grep -iE "dp-2" | grep -iE "\bconnec" | awk '{print $1}')

case $1 in 
    tv)
        xrandr --output $tv --auto
        pacmd set-card-profile 0 output:hdmi-stereo+input:analog-stereo
        notify-send "changed to TV MODE"
        ;;
    monitor)
        notify-send "$tv"
        xrandr --output $tv --primary
        xrandr --output $lap --below $tv --auto
        notify-send "changed to Monitor MODE"
        ;;
    offlap)
        notify-send ">> $lap $thunder"
        xrandr --output $lap --off
        notify-send "Lap Monitor OFF"
        ;;
    lap)
        xrandr --output $lap --auto
        pacmd set-card-profile 0 output:analog-stereo+input:analog-stereo
        notify-send "changed to LAP MODE"
        ;;
    wacom)
        name=$(xsetwacom --list | grep -i styl | awk -F'id' '{ print $1}')
        xsetwacom --set $name MapToOutput $tv
    3mon)
        xrandr --output DP-2-2 --left-of DP-2-1 --primary --output eDP-1 --below DP-2-2  --auto
        pacmd set-card-profile 0 output:hdmi-stereo+input:analog-stereo
esac

