#!/bin/bash

CARD=$(pactl list cards | grep -iE "card #" -A2  | paste -sd " " | awk -F "--" '{print $2 }' | grep -iEo "#." | sed "s|#||g" )
CARD_PROFILES=$(pactl list cards | awk "/Card #$CARD/, /Ports/"  | column -t | awk '/Profiles/,/off/ { print $1}' | head -n -1  | awk 'NR > 1' | sed "s|:||g" )

case $i in
    mic|m)
        pactl set-card-profile  $CARD headset_head_unit
        ;;
    high|h)
        pactl set-card-profile  $CARD headset_head_unit
        ;;
esac
