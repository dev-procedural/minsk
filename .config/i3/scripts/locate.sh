#!/bin/bash

locate home media | rofi -threads 0  -location 0 -dmenu -i -p "locate" -sorting-method fzf -sort  -theme ~/.config/polybar/forest/scripts/rofi/launcher.rasi | xargs -I{}  xdg-open {}

