% vault , vault-0, config

# install vault helm
helm install vault hashicorp/vault --set server.dev.enabled=true --set injector.enabled=true

# enable kubernetes access
vault auth enable kubernetes
vault write auth/kubernetes/config \
   issuer="https://kubernetes.default.svc.cluster.local" \
   token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
   kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
   kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt

# add policy
vault policy write <NAME> - <<'EOF'
path "<PATH_SECRET>" {  
	capabilities = ["<READ_OR_WRITE>"]
}
EOF

# bound to policy
vault write auth/kubernetes/role/<NAME> \
   bound_service_account_names=<SERVICE_ACC> \
   bound_service_account_namespaces=<NAMESPACE> \
   policies=app ttl=24h


# vault list policy 
vault policy list


# vault read policy
vault policy read  <POLICY>

# vault list all roles
vault auth list

# vault list all aws roles
vault list auth/aws/role 

# vault read aws auth role
vault read auth/aws/role/<POLICY>

# vault read aws cred
vault read aws_v2/creds/account_id_<ACCOUNT>

# vault list github teams
vault list auth/github/map/teams/

# vault list approle
vault list auth/approle/role/ 

# vault list roles
vault role list

# vault approle login
vault write auth/approle/login role_id=<VAULT_ROLEID> secret_id=<VAULT_SECRET>

# vault token instrospect
vault token lookup

# debug
vault kv get -output-curl-string secret/<path>

# vault login with aws creds
vault login -method=aws role="<VAULT_AWS_ROLE>" aws_access_key_id="<AWS_KEY_ID>" aws_secret_access_key="<AWS_SECRET_ID>" aws_security_token="<AWS_TOKEN>"
$ VAULT_AWS_ROLE: vault list auth/aws/role/ | awk 'NR > 2' | fzf

# vault get aws creds 
vault kv get aws_v2/creds/<PRJ>
$ PRJ: vault kv list aws_v2/roles | awk 'NR > 2' | fzf

# vault get assume creds
export $(vault kv get -format=json <AUTH>/creds/<PRJ> | jq ".data" |& grep -iE "access|secret|token" | sed "s|access_key|AWS_ACCESS_KEY_ID|g;s|secret_key|AWS_SECRET_ACCESS_KEY|g;s|security_token|AWS_SESSION_TOKEN|g;s|,$||g;s|^\s\+||g;s|\": \"|=|g;s|\"||g")
$ PRJ: vault kv list aws_v2/roles | awk 'NR > 2' | fzf
//$ AUTH: echo "aws_v2\raws_lab aws_v2_app" | fzf

# vault get sts config client
vault list auth/aws/config/sts/ 

# vault create config client
vault write auth/aws/config/sts/<ACCOUNT> sts_role=arn:aws:iam::<ACCOUNT>:role/vault-auth-role
