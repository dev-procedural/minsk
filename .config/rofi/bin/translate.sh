#!/bin/bash 

## Set defaults
export SOURCE="en"               # source language code
export TARGET="es"               # translation language code
MSG="Translate"
THEME="-theme ~/.config/polybar/forest/scripts/rofi/launcher.rasi"

OPTION="
en:es|es:en
" 

ANS=$(echo  $OPTION | rofi $THEME -dmenu -sep '|' -p $MSG -kb-cancel '!Escape')

[[ -z "$ANS" ]] && exit 1

WORD=$(rofi $THEME -dmenu -p $ANS)

[[ -z "$WORD"  ]] && exit 1

#[[ -n "$WORD" ]] && /home/jguerrero/.config/rofi/bin/rofitr $ANS $WORD 
/home/$USER/.config/rofi/bin/rofitr $ANS $WORD 




