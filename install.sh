#!/bin/bash
echo "
 █████╗ ██╗    ██╗███████╗███████╗ ██████╗ ███╗   ███╗███████╗
██╔══██╗██║    ██║██╔════╝██╔════╝██╔═══██╗████╗ ████║██╔════╝
███████║██║ █╗ ██║█████╗  ███████╗██║   ██║██╔████╔██║█████╗  
██╔══██║██║███╗██║██╔══╝  ╚════██║██║   ██║██║╚██╔╝██║██╔══╝  
██║  ██║╚███╔███╔╝███████╗███████║╚██████╔╝██║ ╚═╝ ██║███████╗
╚═╝  ╚═╝ ╚══╝╚══╝ ╚══════╝╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝
                                                              
 ██████╗ ██████╗ ███╗   ██╗███████╗██╗ ██████╗                
██╔════╝██╔═══██╗████╗  ██║██╔════╝██║██╔════╝                
██║     ██║   ██║██╔██╗ ██║█████╗  ██║██║  ███╗               
██║     ██║   ██║██║╚██╗██║██╔══╝  ██║██║   ██║               
╚██████╗╚██████╔╝██║ ╚████║██║     ██║╚██████╔╝               
 ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝     ╚═╝ ╚═════╝                
                                                              "
# intented to work only in ubuntu 20.04

set -eo pipefail 

BRANCH=desktop
TARGET_DIR=~/.minsk

# OS DEPS
PROGS=$(which git curl python3 vim tmux make | wc -l)
[[ ! $PROGS -eq 6 ]] && sudo apt update && sudo apt install git curl python3 vim-gtk tmux make build-essential -y

#I3 STUFF
[[ -d ~/.local/bin ]] && mkdir -p ~/.local/bin

sudo add-apt-repository ppa:kgilmer/speed-ricer -y
sudo add-apt-repository ppa:mmstick76/alacritty -y
sudo apt-get update

echo "INSTALLING DEPS"
sudo apt install -y rofi rustc flameshot \
                    fonts-powerline byzanz \
                    wmctrl xdotool libinput-tools \
                    xsel alacritty polybar \
                    jq slop xsel i3-wm vim-gtk \
                    silversearcher-ag gdebi \
                    tesseract-ocr

sudo apt install -y i3-gaps 

# GREENCLIP
curl -Lso ~/.local/bin/greenclip https://github.com/erebe/greenclip/releases/download/4.1/greenclip
chmod +x ~/.local/bin/greenclip

sudo ln -s ~/.local/bin/greenclip /usr/local/bin

# TMUX TPM
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# LIB INPUT
git clone https://github.com/bulletmark/libinput-gestures.git /tmp/libinput
cd /tmp/libinput
sudo make install
sudo gpasswd -a $USER input 
libinput-gestures-setup autostart start

# POLYBAR
~/.config/polybar/setup.sh
sudo curl -Lo /usr/local/bin/networkmanager_dmenu https://raw.githubusercontent.com/firecat53/networkmanager-dmenu/main/networkmanager_dmenu 
chmod +x /usr/local/bin/networkmanager_dmenu

# ROFITR
curl -Lo /tmp/crow.deb https://github.com/crow-translate/crow-translate/releases/download/2.8.4/crow-translate_2.8.4_amd64.deb
sudo dpkg -i /tmp/crow.deb

# ENDI3 STUFF


# OS
OS=$(cat /etc/os-release | grep -i '^ID=' | cut -d\= -f2)
VIM=$(vim --version | head -1 | awk '{ print $5}')

## DEPS
# FZF 
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install --all

# NAVI
BIN_DIR=$HOME/.local/bin bash <(curl -sL https://raw.githubusercontent.com/denisidoro/navi/master/scripts/install)

# GOLANG 
bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)

# FONTS
git clone https://github.com/sebastiencs/icons-in-terminal.git /tmp/icons
/tmp/icons/install.sh

# RANGER
$(which pip3 || which pip ) install ranger-fm ueberzug  
git clone https://github.com/laggardkernel/ranger-fzf-marks.git ~/.config/ranger/plugins/fzf-marks
git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons

# BOTTOM
cargo install bottom


# todo add bashmarks
[[ ! -f /tmp/minsk ]] && \
git clone -b $BRANCH https://gitlab.com/dev-procedural/minsk.git $TARGET_DIR

# install correct vim 8
shopt -s nocasematch
STRING=$(echo awk "BEGIN { print ( $VIM >= 8.1) ? 'YES' : 'NO' }")
STRING=$(echo $STRING | sed "s|'|\"|g")
STRING=$(echo $STRING | sed "s|BEGIN|'BEGIN|g;s|\}|\}'|g" | sh)

#install dotfiles
BASHRC_ENTRY='
for file in ~/.bashrc.d/*;
do
     [[ -L  $file && -f $file ]]  && source "$file"
done
'

echo "$BASHRC_ENTRY" >> ~/.bashrc
bash $TARGET_DIR/install

vim +PlugInstall +qall +silent 

# apt install locales-all

# reset
