" vim-bootstrap b0a75e4

"*****************************************************************************
"" Vim-PLug core
"*****************************************************************************
if has('vim_starting')
  set nocompatible               " Be iMproved
endif

let vimplug_exists=expand('~/.vim/autoload/plug.vim')

let g:vim_bootstrap_langs = "c,elixir,go,haskell,html,javascript,php,python,ruby"
let g:vim_bootstrap_editor = "vim"				" nvim or vim

if !filereadable(vimplug_exists)
  if !executable("curl")
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."
  echo ""
  silent !\curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  let g:not_finish_vimplug = "yes"

  autocmd VimEnter * PlugInstall
endif

" Required:
call plug#begin(expand('~/.vim/plugged'))

"*****************************************************************************
"" Plug install packages
"*****************************************************************************
Plug 'posva/vim-vue'
" Plug 'voldikss/vim-floaterm'
" Plug 'tpope/vim-repeat', { 'branch': 'master' }
" Plug 'reireias/vim-tfsec', { 'branch': 'main' }
Plug 'kshenoy/vim-signature'
Plug 'mhinz/vim-startify', { 'branch': 'master' }
Plug 'jpalardy/vim-slime', { 'branch': 'main' }
Plug 'frazrepo/vim-rainbow'
Plug 'tidalcycles/vim-tidal'
Plug 'junegunn/rainbow_parentheses.vim'
" Plug 'kaicataldo/material.vim', { 'branch': 'main' }
" Plug 'jvanja/vim-bootstrap4-snippets'
" Plug 'pseewald/vim-anyfold'
" Plug 'jistr/vim-nerdtree-tabs'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
" Plug 'vim-scripts/grep.vim'
" Plug 'vim-scripts/CSApprox'
" Plug 'bronson/vim-trailing-whitespace'
" Plug 'Raimondi/delimitMate'
" Plug 'majutsushi/tagbar'
Plug 'scrooloose/syntastic'
" Plug 'Yggdroot/indentLine'
" Plug 'avelino/vim-bootstrap-updater'
Plug 'sheerun/vim-polyglot'
" Plug 'scrooloose/vim-slumlord'
" Plug 'aklt/plantuml-syntax'
" Plug 'MarcWeber/vim-addon-mw-utils'
" Plug 'tomtom/tlib_vim'
" Plug 'garbas/vim-snipmate'
Plug 'SirVer/ultisnips'
" Plug 'terryma/vim-multiple-cursors'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" Plug 'townk/vim-autoclose'
" Plug 'arnar/vim-matchopen'
" Plug 'Yilin-Yang/vim-markbar'
" Plug 'junegunn/vim-peekaboo'
" Plug 'shumphrey/fugitive-gitlab.vim'
" Plug 'bluz71/vim-moonfly-colors'
" Plug 'mdempsky/gocode', { 'rtp': 'vim', 'do': '~/.vim/plugged/gocode/vim/symlink.sh' }
" " GOdevelopment
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" Plug 'hashivim/vim-terraform'
Plug 'AndrewRadev/linediff.vim'

Plug 'puremourning/vimspector'

" close html tags
" Plug 'Valloric/MatchTagAlways' 
" let g:mta_use_matchparen_group = 1

let g:mta_filetypes = {
    \ 'html' : 1,
    \ 'xhtml' : 1,
    \ 'xml' : 1,
    \ 'jinja' : 1,
    \ 'htmldjango': 1,
    \}

let g:make = 'gmake'
if exists('make')
        let g:make = 'make'
endif
Plug 'Shougo/vimproc.vim', {'do': g:make}

"" Vim-Session
Plug 'xolox/vim-misc'
Plug 'xolox/vim-session'

if v:version >= 703
  Plug 'Shougo/vimshell.vim'
endif

if v:version >= 704
  "" Snippets
  Plug 'SirVer/ultisnips'
endif

"" Color
" Plug 'tomasr/molokai'


" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/UltiSnips/langs', $HOME.'/.vim/UltiSnips', $HOME.'/.vim/plugged/vim-bootstrap4-snippets/UltiSnips', $HOME.'/.vim/plugged/aws-vim', $HOME.'/.vim/UltiSnips/infra', $HOME.'/.vim/UltiSnips/arena', $HOME.'/.vim/UltiSnips/mzn', $HOME.'/.vim/UltiSnips/k8s', $HOME.'/.vim/UltiSnips/cicd', $HOME.'/.vim/UltiSnips/music']


" Plug 'MarcWeber/vim-addon-mw-utils'
" Plug 'tomtom/tlib_vim'
" Plug 'garbas/vim-snipmate'

let g:syntastic_yaml_checkers = ['yamllint']


"*****************************************************************************
"" Custom bundles
"*****************************************************************************

" html
"" HTML Bundle
" Plug 'hail2u/vim-css3-syntax'
" Plug 'gorodinskiy/vim-coloresque'
" Plug 'tpope/vim-haml'
" Plug 'mattn/emmet-vim'


" java
" Plug 'artur-shaik/vim-javacomplete2'


" javascript
"" Javascript Bundle
Plug 'jelera/vim-javascript-syntax'



"*****************************************************************************
"*****************************************************************************

" let g:neosnippet#snippets_directory='~/.vim/snippets/'


"" Include user's extra bundle
if filereadable(expand("~/.vimrc.local.bundles"))
  source ~/.vimrc.local.bundles
endif

call plug#end()

"" Required:
filetype plugin indent on


"*****************************************************************************
"" Basic Setup
"*****************************************************************************"
"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set bomb
set binary
set ttyfast
set shortmess-=S

"" Fix backspace indent
set backspace=indent,eol,start


"" Map leader to ,
let mapleader=','

"" Enable hidden buffers
set hidden

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

"" Pasting indent
" set paste

"" Directories for swp files
set nobackup
set noswapfile

set fileformats=unix,dos,mac

if exists('$SHELL')
    set shell=$SHELL
else
    set shell=/bin/sh
endif

" session management
let g:session_directory = "~/.vim/session"
let g:session_autoload = "no"
let g:session_autosave = "no"
let g:session_command_aliases = 1



"*****************************************************************************
"" Visual Settings
"*****************************************************************************
syntax on
set ruler
set number

let no_buffers_menu=1
if !exists('g:not_finish_vimplug')
  colorscheme atom-dark-256
endif

set mousemodel=popup
set t_Co=256
set guioptions=egmrti
set gfn=Monospace\ 10

if has("gui_running")
  if has("gui_mac") || has("gui_macvim")
    set guifont=Menlo:h12
    set transparency=7
  endif
else
  let g:CSApprox_loaded = 1

  " IndentLine
  let g:indentLine_enabled = 1
  let g:indentLine_concealcursor = 0
  let g:indentLine_char = ' '
  let g:indentLine_faster = 1

  
  if $COLORTERM == 'gnome-terminal'
    set term=gnome-256color
  else
    if $TERM == 'xterm'
      set term=xterm-256color
    endif
  endif
  
endif


if &term =~ '256color'
  set t_ut=
endif


"" Disable the blinking cursor.
set gcr=a:blinkon0
set scrolloff=3

"" Status bar
set laststatus=2

"" Use modeline overrides
set modeline
set modelines=10

set title
set titleold="Terminal"
set titlestring=%F

set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

if exists("*fugitive#statusline")
  set statusline+=%{fugitive#statusline()}
endif

" vim-airline
let g:airline_theme = 'powerlineish'
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline_skip_empty_sections = 1
let g:airline_statusline_ontop=1 
let g:syntastic_quiet_messages = { "level": "warnings" }


"*****************************************************************************
"" Abbreviations
"*****************************************************************************
"" no one is really happy until you have this shortcuts
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall

"" NERDTree configuration
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:NERDTreeQuitOnOpen=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeWinSize = 50
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
nnoremap <silent> <F2> :NERDTreeFind<CR>
nnoremap <silent> <F3> :NERDTreeToggle<CR>

" grep.vim
" nnoremap <silent> <leader>f :Rgrep<CR>
let Grep_Default_Options = '-IR'
let Grep_Skip_Files = '*.log *.db'
let Grep_Skip_Dirs = '.git node_modules'

" vimshell.vim
let g:vimshell_user_prompt = 'fnamemodify(getcwd(), ":~")'
let g:vimshell_prompt =  '$ '

" terminal emulation
if g:vim_bootstrap_editor == 'nvim'
  nnoremap <silent> <leader>sh :terminal<CR>
else
  nnoremap <silent> <leader>sh :VimShellCreate<CR>
endif

"*****************************************************************************
"" Functions
"*****************************************************************************
if !exists('*s:setupWrapping')
  function s:setupWrapping()
    set wrap
    set wm=2
    set textwidth=79
  endfunction
endif

"*****************************************************************************
"" Autocmd Rules
"*****************************************************************************
"" The PC is fast enough, do syntax highlight syncing from start unless 200 lines
augroup vimrc-sync-fromstart
  autocmd!
  autocmd BufEnter * :syntax sync maxlines=200
augroup END

"" Remember cursor position
augroup vimrc-remember-cursor-position
  autocmd!
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

"" txt
augroup vimrc-wrapping
  autocmd!
  autocmd BufRead,BufNewFile *.txt call s:setupWrapping()
augroup END

"*****************************************************************************
"" Mappings
"*****************************************************************************

"" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>

"" Git
" noremap <Leader>ga :Gwrite<CR>
" noremap <Leader>gc :Gcommit<CR>
" noremap <Leader>gsh :Gpush<CR>
" noremap <Leader>gll :Gpull<CR>
" noremap <Leader>gs :Gstatus<CR>
" noremap <Leader>gb :Gblame<CR>
" noremap <Leader>gd :Gvdiff<CR>
" noremap <Leader>gr :Gremove<CR>

" session management
nnoremap <leader>so :OpenSession<Space>
nnoremap <leader>ss :SaveSession<Space>
nnoremap <leader>sd :DeleteSession<CR>
nnoremap <leader>sc :CloseSession<CR>

"" Tabs
nnoremap <Tab> :Windows<CR>
" nnoremap <Tab> gt
nnoremap <S-Tab> :Buffers<CR>
" nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

"" Set working directory
nnoremap <leader>. :lcd %:p:h<CR>

"" Opens an edit command with the path of the currently edited file filled in
noremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

"" Opens a tab edit command with the path of the currently edited file filled
noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>

"" fzf.vim
set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__,vendor
" let $FZF_DEFAULT_COMMAND =  "find * -prune -o -path '__pycache__/**' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o -path 'vendor/**'  -prune -o  -type f -print -o -type l -print 2> /dev/null"
" let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path '__pycache__/**' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o -path 'vendor/**'  -prune -o  -type f -print -o -type l -print 2> /dev/null"

" let $FZF_DEFAULT_COMMAND='find . \! \( -type d -path ./.git -prune \) \! -type d \! -name ''*.tags'' -printf ''%P\n'''
let $FZF_DEFAULT_OPTS = '--height 40% --layout=reverse --border --bind ctrl-a:select-all  --bind ctrl-y:preview-up,ctrl-e:preview-down'

" The Silver Searcher
if executable('ag')
  let $FZF_DEFAULT_COMMAND = 'ag --hidden --ignore .git -g ""'
  set grepprg=ag\ --nogroup\ --nocolor
endif

" cnoremap <C-P> <C-R>=expand("%:p:h") . "/" <CR>
" nnoremap <silent> <leader>b :Buffers<CR>
" nnoremap <silent> <leader>e :FZF -m<CR>

" snippets
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"
let g:UltiSnipsEditSplit="vertical"

" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list=1
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol=' '
let g:syntastic_style_error_symbol = '✗'
let g:syntastic_style_warning_symbol = ' '
let g:syntastic_auto_loc_list=1
let g:syntastic_aggregate_errors = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_auto_jump = 2

let g:syntastic_tex_checkers = ['lacheck', 'text/language_check']


" Tagbar
" nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_autofocus = 1

" Disable visualbell
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif

"" Copy/Paste/Cut
if has('unnamedplus')
  set clipboard=unnamed,unnamedplus
endif

noremap YY "+y<CR>
noremap <leader>p "+gP<CR>
noremap XX "+x<CR>

if has('macunix')
  " pbcopy for OSX copy/paste
  vmap y :!xsel -i -b<CR>
endif

"" Close buffer
noremap <leader>c :bd<CR>

"" Clean search (highlight)
nnoremap <silent> <leader><space> :noh<cr>

"" Switching windows
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h

"" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

"" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

"" Open current line on GitHub
nnoremap <Leader>o :.GBrowse<CR>

"*****************************************************************************
"" Custom configs
"*****************************************************************************

" c
autocmd FileType c setlocal tabstop=4 shiftwidth=4 expandtab
autocmd FileType cpp setlocal tabstop=4 shiftwidth=4 expandtab


" elixir


" go
" vim-go
" run :GoBuild or :GoTestCompile based on the go file
" function! s:build_go_files()
"   let l:file = expand('%')
"   if l:file =~# '^\f\+_test\.go$'
"     call go#test#Test(0, 1)
"   elseif l:file =~# '^\f\+\.go$'
"     call go#cmd#Build(0)
"   endif
" endfunction

" GOLANG 
let g:go_list_type = "quickfix"
let g:go_fmt_command = "goimports"
let g:go_fmt_fail_silently = 1
let g:syntastic_go_checkers = ['golint', 'govet']
let g:syntastic_mode_map = { 'mode': 'passive', 'passive_filetypes': ['go'] }

let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_structs = 1
let g:go_highlight_generate_tags = 1
let g:go_highlight_space_tab_error = 0
let g:go_highlight_array_whitespace_error = 0
let g:go_highlight_trailing_whitespace_error = 0
let g:go_highlight_extra_types = 1

let g:go_info_mode='gopls'
let g:go_def_mode='gopls'


" autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4

augroup completion_preview_close
  autocmd!
  if v:version > 703 || v:version == 703 && has('patch598')
    autocmd CompleteDone * if !&previewwindow && &completeopt =~ 'preview' | silent! pclose | endif
  endif
augroup END

" html
" for html files, 2 spaces
autocmd Filetype html setlocal ts=2 sw=2 expandtab

"autocmd FileType Vagrantfile set UltiSnipsAddFiletypes Vagrant

" javascript
let g:javascript_enable_domhtmlcss = 1

" vim-javascript
augroup vimrc-javascript
  autocmd!
  autocmd FileType javascript set tabstop=4|set shiftwidth=4|set expandtab softtabstop=4
augroup END


" python
" vim-python
augroup vimrc-python
  autocmd!
  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=4
      \ formatoptions+=croq softtabstop=4
      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

" syntastic
" flake8 avoid
nnoremap <silent> <leader>sy :SyntasticToggleMode<CR>
let g:syntastic_python_python_exec = 'python3'
let g:syntastic_python_checkers=['python']

let g:syntastic_python_flake8_exec = 'python3'
let g:syntastic_python_flake8_args = ['-m', 'flake8']

" vim-airline
let g:airline#extensions#virtualenv#enabled = 1

" Syntax highlight
" Default highlight is better than polyglot
" let g:polyglot_disabled = ['python']
let python_highlight_all = 1


nmap <Leader>mm <Plug>ToggleMarkbar
" the following are unneeded if ToggleMarkbar is mapped
nmap <Leader>mo <Plug>OpenMarkbar
nmap <Leader>mc <Plug>CloseMarkbar
"*****************************************************************************
"*****************************************************************************

"" Include user's local vim config
if filereadable(expand("~/.vimrc.local"))
  source ~/.vimrc.local
endif

"*****************************************************************************
"" Convenience variables
"*****************************************************************************

" vim-airline
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

if !exists('g:airline_powerline_fonts')
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline_left_sep          = '▶'
  let g:airline_left_alt_sep      = '»'
  let g:airline_right_sep         = '◀'
  let g:airline_right_alt_sep     = '«'
  let g:airline#extensions#branch#prefix     = ''
  let g:airline#extensions#readonly#symbol   = '⊘'
  let g:airline#extensions#linecolumn#prefix = '¶'
  let g:airline#extensions#paste#symbol      = 'ρ'
  let g:airline_symbols.linenr    = '␊'
  let g:airline_symbols.branch    = ''
  let g:airline_symbols.paste     = 'ρ'
  let g:airline_symbols.paste     = 'Þ'
  let g:airline_symbols.paste     = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
else
  let g:airline#extensions#tabline#left_sep = ''
  let g:airline#extensions#tabline#left_alt_sep = ''

  " powerline symbols
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
endif


if has('persistent_undo')      "check if your vim version supports it
  set undofile                 "turn on the feature  
  set undodir=$HOME/.vim/undo  "directory where the undo files will be stored
endif

"" Custom Ultisnips
" autocmd FileType Vagrantfile UltiSnipsAddFiletypes Vagrantfile
autocmd FileType service     UltiSnipsAddFiletypes systemd
" autocmd FileType cfn         UltiSnipsAddFiletypes cfn
" autocmd FileType app.py      UltiSnipsAddFiletypes flask
" autocmd FileType sp          UltiSnipsAddFiletypes sp 

" let g:ackprg = "ag --vimgrep"

" Make double-<Esc> clear search highlights
nnoremap <silent> <Esc><Esc> <Esc>:nohlsearch<CR><Esc>

" _____________________________________________[ FZF 

" Open FZF
nnoremap <silent> <leader>f <Esc>:FZF<CR>
let g:fzf_layout = { 'up': '~100%' }

" Open AG (r)egex
nnoremap <silent> <leader>r :Ag<CR>

" List (S)nippets
nnoremap <silent> <leader>S :Snippets<CR> 

" List (s)earch
nnoremap <silent> <leader>s :Lines<CR>
"
" List (b)uffers
nnoremap <silent> <leader>b :Buffers<CR>

" List search (B)lines
nnoremap <silent> <leader>B :BLines<CR>
"
" List search (H)istory
nnoremap <silent> <leader>hh :History<CR>
"
" List search (H)istory
nnoremap <silent> <leader>ta :Tags<CR>

" (L)ocate
nnoremap <leader>l :Locate<space>


" _____________________________________________[ Other MAPS
" List search (C)ustom yank search
" nnoremap <silent> <leader><CR>

" List search (C)ustom yank search
nnoremap <silent> <leader>p q/p<CR>

" Cd dir (CD) buffer dir
nnoremap <silent> <leader>cd :cd %:h<CR>

nnoremap <silent> <leader>diff :windo diffthis<CR>

" noremap <C-Up> :m .-2<CR>
" noremap <C-Down> :m .+2<CR>

let g:syntastic_disabled_filetypes=['yaml','yml']

" nnoremap <F3> :call win_execute(win_getid(winnr('#')), "normal! \<C-E>")
" nnoremap <F1> <C-W><C-W><C-E><C-W><C-W>
" nnoremap <F2> <C-W><C-W><C-Y><C-W><C-W>

nnoremap <silent> <leader>cd.. :cd ..<CR>

nnoremap <silent> <leader>ii :set tabstop=2 softtabstop=0 shiftwidth=2 expandtab<CR>

"_____________________________________________[ Color override
hi MatchParen ctermbg=7
hi Search ctermbg=12
hi Pmenu ctermbg=12
" hi CocHighlightRead ctermbg=12


"" Tabs. May be overriten by autocmd rules
set tabstop=2
set softtabstop=0
set shiftwidth=2
set expandtab

"______________________________________________[ SLIME
let g:slime_target = "tmux"
let g:slime_default_config = {"socket_name": get(split($TMUX, ","), 0), "target_pane": "1"}
let g:slime_preserve_curpos = 0
let g:slime_cell_delimiter = "#%%"
let g:slime_python_ipython = 1
nmap <leader>sss <Plug>SlimeSendCell

" rainbow
let g:rainbow_active = 1
let g:rainbow_guifgs = ['RoyalBlue3', 'DarkOrange3', 'DarkOrchid3', 'FireBrick']
let g:rainbow_ctermfgs = ['lightblue', 'lightgreen', 'yellow', 'red', 'magenta']

set tabstop=2 softtabstop=0 shiftwidth=2 expandtab
set ic

" q / p
"
if &term == "screen-256color"
  nnoremap <Esc>[1;2C <C-Right>
  nnoremap <Esc>[1;2D <C-Left>
  nnoremap <Esc>[1;4C <C-Right>
  nnoremap <Esc>[1;4D <C-Left>
  nnoremap <Esc>[1;5C <C-Right>
  nnoremap <Esc>[1;5D <C-Left>
  nnoremap <Esc>[1;7C <C-Right>
  nnoremap <Esc>[1;7D <C-Left>
  nnoremap <Esc>[1;4B <Down>
  nnoremap <Esc>[1;4A <Up>
  inoremap <Esc>[1;5C <C-Right>
  inoremap <Esc>[1;5D <C-Left>
  inoremap <Esc>[1;7C <C-Right>
  inoremap <Esc>[1;7D <C-Left>
  inoremap <Esc>[1;7D <C-Left>
  inoremap <Esc>[1;4B <Down>
  inoremap <Esc>[1;4A <Up>
endif

" WSL yank support
" let s:clip = '/mnt/c/Windows/System32/clip.exe'  " change this path according to your mount point
" if executable(s:clip)
"     augroup WSLYank
"         autocmd!
"         autocmd TextYankPost * if v:event.operator ==# 'y' | call system(s:clip, @0) | endif
"     augroup END
" endif

let g:SignatureMap = {
        \ 'GotoNextLineByPos'  :  "mt",
        \ 'GotoPrevLineByPos'  :  "mT"
        \ }
"
" command! -nargs=+ Sub execute 's/' . split(<q-args>, ' ')[0] . '/' . split(<q-args>, ' ')[1] . '/g'
" command! -nargs=+ Yhere execute call('printf', ['s/%s/%s/g'] + split(<q-args>, ' '))
" command! -nargs=+ Yhere execute call('printf', ['%dco.'] + split(<q-args>, ' '))
command! LS call fzf#run(fzf#wrap({'source': 'ls'}))
command! -bang LS call fzf#run(fzf#wrap({'source': 'ls'}, <bang>0))
