autocmd BufNewFile,BufRead .gitlab-ci.yml set filetype=gitlabci
autocmd FileType gitlabci UltiSnipsAddFiletypes gitlabci
